//
//  LoginViewController.swift
//  SuperCard
//
//  Created by Joaquin Rocco on 22/04/2016.
//  Copyright © 2016 Joaquin Rocco. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var mainContentView: UIView!
    
    var bottomPadding: CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailField.delegate = self
        passwordField.delegate = self
        
        bottomPadding = self.view.frame.height - (mainContentView.bounds.origin.y+mainContentView.bounds.height)
        
        // Do any additional setup after loading the view.
        
        errorLabel.hidden = true
        
    }
    
    var registeredUsers = ["ucu@ucu.com":"ucu2016"]

    @IBAction func login(sender: AnyObject) {
        
        errorLabel.hidden = false
        
        var loginSuccess = false
        
        let mail = emailField.text
        let pass = passwordField.text
        if let mail = mail, pass = pass{
            if let correctPassword = registeredUsers[mail]{
                if pass == correctPassword{
                    loginSuccess = true
                }
            }
        }
        
        if loginSuccess{           performSegueWithIdentifier("showHome", sender: nil)
        }else{
            errorLabel.text = "Wrong username or password"
            errorLabel.hidden = false
        }
    }
    
    // Manage keyboard
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.emailField{
            self.passwordField.becomeFirstResponder()
        }else{
        self.view.endEditing(true)
            login(self)
        }
        return false
    }
    
    override func viewWillAppear(animated: Bool) {
        self.startKeyboardObserver()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.stopKeyboardObserver()
    }
    
    
    private func startKeyboardObserver(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil) //WillShow and not Did ;) The View will run animated and smooth
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func stopKeyboardObserver() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize: CGSize =    userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size {
                
                let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height,  0.0);
                
                self.scrollView.contentInset = contentInset
                self.scrollView.scrollIndicatorInsets = contentInset
                
                self.scrollView.contentOffset = CGPointMake(self.scrollView.contentOffset.x, 0 + keyboardSize.height-bottomPadding!) //set zero instead self.scrollView.contentOffset.y
                
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let _: CGSize =  userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size {
                let contentInset = UIEdgeInsetsZero;
                
                self.scrollView.contentInset = contentInset
                self.scrollView.scrollIndicatorInsets = contentInset
                self.scrollView.contentOffset = CGPointMake(self.scrollView.contentOffset.x, self.scrollView.contentOffset.y)
            }
        }
    }
}
