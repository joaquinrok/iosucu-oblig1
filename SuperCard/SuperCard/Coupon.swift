//
//  Coupon.swift
//  SuperCard
//
//  Created by SP 25 on 21/4/16.
//
//

import UIKit

class Coupon {
    
    var id:Double
    var productImageName: String
    var title: String
    var subTitle: String
    
    init(title: String, subTitle: String, productImageName: String){
        self.id = Double(arc4random())
        self.productImageName = productImageName
        self.title = title
        self.subTitle = subTitle
    }
    
}
