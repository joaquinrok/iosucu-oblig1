//
//  BigButton.swift
//  SuperCard
//
//  Created by Joaquin Rocco on 15/04/2016.
//  Copyright © 2016 Joaquin Rocco. All rights reserved.
//

import UIKit
import Foundation

@IBDesignable
class RoundedButton: UIButton{
    
    @IBInspectable
    var rounded: Bool = true{
        didSet{
            //layer.borderWidth = rounded ? 1 : 0
            layer.cornerRadius = rounded ? 10 : 0
            //layer.borderColor = layer.backgroundColor
        }
    }
    
    @IBInspectable
    var backgroundFillColor: UIColor?{
        didSet{
            layer.backgroundColor = backgroundFillColor?.CGColor
        }
    }
}
