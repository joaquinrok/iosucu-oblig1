//
//  DetailsViewController.swift
//  SuperCard
//
//  Created by SP 25 on 21/4/16.
//
//

import UIKit

class DetailsViewController: UIViewController {
    
    var coupon:Coupon?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var qrImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let coupon = coupon{
            titleLabel.text = coupon.title
            subTitleLabel.text = coupon.subTitle
            
            //QR
            let qrCode = QRCode(coupon.id.description)
            qrImageView.image = qrCode?.image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
