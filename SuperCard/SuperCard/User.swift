//
//  User.swift
//  SuperCard
//
//  Created by Joaquin Rocco on 22/04/2016.
//  Copyright © 2016 Joaquin Rocco. All rights reserved.
//

import Foundation

class User{
    
    var firstName: String
    var lastName: String
    var email: String
    
    private var pts: Int = 0
    var points:String{
        get{
            pts += Int(arc4random_uniform(50))
            return "\(pts) points"
        }
    }
    
    init(firstName:String, lastName:String, email:String){
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
    }
    
    var fullname: String{
        return firstName + " " + lastName
    }
}