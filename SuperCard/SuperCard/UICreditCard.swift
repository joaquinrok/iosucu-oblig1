//
//  UICreditCard.swift
//  SuperCard
//
//  Created by SP 25 on 21/4/16.
//
//

import UIKit

@IBDesignable
class UICreditCard: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        _setup()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        _setup()
    }
    
    override func prepareForInterfaceBuilder() {
        _setup()
    }
    
    private func _setup() {
        imageView?.alpha = 0.5
    }

}
