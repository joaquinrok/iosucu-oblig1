//
//  HomeViewController.swift
//  SuperCard
//
//  Created by Joaquin Rocco on 22/04/2016.
//  Copyright © 2016 Joaquin Rocco. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var user: User = User(firstName: "Juan", lastName: "Perez de La Fuente", email: "jperezdelafuelte@gmail.com")
    
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var centerContainer: UIView!
    @IBOutlet weak var cardContainer: UIView!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    @IBOutlet weak var creditCardNameLabel: UILabel!
    
    //var qrCodeImageView: UIImageView?
    var showingCard = true
    
    override func viewDidLoad() {
        
        pointsLabel.text = user.points
        
        creditCardNameLabel.text = user.fullname
        let qrCode = QRCode(user.email)
    
        qrCodeImageView!.image = qrCode?.image
        qrCodeImageView?.hidden = true
        
        centerContainer.addSubview(qrCodeImageView!)
        
        let timer = NSTimer(timeInterval: 1.0, target: self, selector: #selector(HomeViewController.updatePoints), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
        
    }
    
    func updatePoints(){
        pointsLabel.text = user.points
    }
    
    @IBAction func cardTapped(sender: UITapGestureRecognizer) {
        if (showingCard) {
            UIView.transitionFromView(cardContainer, toView: qrCodeImageView!, duration: 1, options: [UIViewAnimationOptions.TransitionFlipFromLeft,UIViewAnimationOptions.ShowHideTransitionViews], completion: nil)
            showingCard = false
        } else {
            UIView.transitionFromView(qrCodeImageView!, toView: cardContainer, duration: 1, options: [UIViewAnimationOptions.TransitionFlipFromLeft,UIViewAnimationOptions.ShowHideTransitionViews], completion: nil)
            showingCard = true
        }
    }
}
