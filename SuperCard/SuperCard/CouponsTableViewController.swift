//
//  CouponsTableViewController.swift
//  SuperCard
//
//  Created by SP 25 on 21/4/16.
//
//

import UIKit

class CouponsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    var coupons = [Coupon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Load dummy data
        coupons.append(Coupon(title: "10% OFF", subTitle: "In any Coca-Cola 330ml can six-pack or eight-pack", productImageName: "product_coke"))
        coupons.append(Coupon(title: "15% OFF", subTitle: "In any Coca-Cola 330ml can bought after 3pm", productImageName: "product_coke"))
        coupons.append(Coupon(title: "1+1 free", subTitle: "In Coke cans which are red", productImageName: "product_coke"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return coupons.count
    }

    
    private struct StoryBoard{
        static let CouponCell = "couponCell"
        static let ShowCouponDetailsSegue = "showCouponDetails"
    }
    
    func getCoupon(forIndexPath indexPath: NSIndexPath) -> Coupon{
        return coupons[indexPath.row]
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(StoryBoard.CouponCell, forIndexPath: indexPath)

        let coupon = getCoupon(forIndexPath: indexPath)
        
        if let cell = cell as? CouponTableViewCell{
            cell.productImageView.image = UIImage(named: coupon.productImageName)
            cell.titleLabel.text = coupon.title
            cell.subTitleLabel.text = coupon.subTitle
        }

        return cell
    }
    
    var selectedCoupon: Coupon?
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedCoupon = getCoupon(forIndexPath: indexPath)
        
        performSegueWithIdentifier(StoryBoard.ShowCouponDetailsSegue, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == StoryBoard.ShowCouponDetailsSegue{
            if let detailsVC = segue.destinationViewController as? DetailsViewController{
                if let coupon = selectedCoupon{
                    detailsVC.coupon = coupon
                }
            }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
